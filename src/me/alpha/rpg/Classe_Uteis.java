package me.alpha.rpg;


import java.util.Random;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import com.darkblade12.particleeffect.ParticleEffect;

public class Classe_Uteis implements Listener {

	public static double XpGanho = 0 ;
	
	
	@EventHandler
	public void TomarDano(EntityDamageEvent e ){
		
		ParticleEffect.REDSTONE.display(0.3f, 1, 0.3f, 0, 30, e.getEntity().getLocation(), 500);
		
	}
			

	@EventHandler(priority=EventPriority.HIGH)
	
   public void DarDano(EntityDamageEvent e){
		
		Entity ent = e.getEntity();
	
		
		if (ent instanceof LivingEntity ){
			if (e.getDamage() >= ((LivingEntity) ent).getHealth()){
				if (e.getCause()==DamageCause.ENTITY_ATTACK || e.getCause() ==DamageCause.PROJECTILE || e.getCause()==DamageCause.MAGIC  ){
				Random rand = new Random();
				
			    double xpGanho = 12+rand.nextInt(20)*0.4;
			    XpGanho = xpGanho;
				Util.XpGeral= Util.XpGeral + xpGanho;
				
				if (!(ent instanceof Player)){
					e.setCancelled(true);
					ent.remove();
					ParticleEffect.REDSTONE.display(0.5f, 1, 0.5f, 10, 100, e.getEntity().getLocation(), 500);
				}else {
					
					e.setCancelled(true);
					e.getEntity().teleport(e.getEntity().getWorld().getSpawnLocation());
					((LivingEntity) ent).setHealth(20);
				}
				
					
			}
		}
		}
	
}
	@EventHandler
		public void AtualizarScoreBoard(PlayerMoveEvent e){
		
		Player player = e.getPlayer();
		
		
		ScoreboardManager manager = Bukkit.getScoreboardManager(); 
Scoreboard score = manager.getNewScoreboard();
Objective obj = score.registerNewObjective("obj", "dummy");
obj.setDisplaySlot(DisplaySlot.SIDEBAR);
obj.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"RPGEssentials");
Score sc0 = obj.getScore(ChatColor.BLACK+"----------------------");
sc0.setScore(43);
Score sc = obj.getScore(ChatColor.AQUA+""+ChatColor.BOLD+"   Xp: "+(int)(Util.XpGeral)+" | "+(int)(Util.inixp));
sc.setScore(42);
Score sc2 = obj.getScore(ChatColor.GREEN+""+ChatColor.BOLD+"   Level : "+Util.Level);
sc2.setScore(40);
player.setScoreboard(score);

Team team = score.registerNewTeam("RPGEssential");
team.addPlayer(player);
team.setPrefix(ChatColor.BOLD+""+ChatColor.YELLOW+"["+ChatColor.RED+Util.Level+ChatColor.YELLOW+"]"+ChatColor.WHITE);
team.setDisplayName("Test");

			
		}	
	
	
	
	
	
	
	@EventHandler
	public void NaoquebrarBlocosComEspada(BlockBreakEvent e){
		
		
		Player player = e.getPlayer();
		
		
		
		
		
		
		if(player.getItemInHand().getType()==Material.DIAMOND_SWORD||player.getItemInHand().getType()==Material.DIAMOND_SPADE||
				player.getItemInHand().getType()==Material.DIAMOND_AXE||player.getItemInHand().getType()==Material.IRON_SWORD||
				player.getItemInHand().getType()==Material.IRON_SPADE||player.getItemInHand().getType()==Material.IRON_AXE||
				player.getItemInHand().getType()==Material.STONE_SWORD||player.getItemInHand().getType()==Material.STONE_SPADE||
				player.getItemInHand().getType()==Material.STONE_AXE){
			
			
			e.setCancelled(true);
			
		}
		
	}
	
}